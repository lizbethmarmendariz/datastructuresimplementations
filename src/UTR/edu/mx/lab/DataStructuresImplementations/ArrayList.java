/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UTR.edu.mx.lab.DataStructuresImplementations;
import UTR.edu.mx.lab.datastructures.List;
import java.util.Arrays;

/**
 * 
 * @author Lizbeth Monserrath Plascencia Armendariz
 */
public class ArrayList implements List {

    private Object[] elements;
    private int size;

    public ArrayList() {
        this(10);
    }

    /**
     * An ordered collection (also known as a sequence).
     */
    public ArrayList(int inicialCapacity) {
        elements = new Object[inicialCapacity];
    }

    /**
     * Appends the specified element to the end of this list.
     */
    public boolean add(Object element) {
        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     */
    @Override
    public void add(int index, Object element) {
        rangeCheckForAdd(index);
        ensureCapacity(size + 1);
        for (int i = size -1 ; i >= index; i--){
            elements [i + 1] = elements [i];
        }
        elements[index] = element;
        size++;
    }

    /**
     * Removes all of the elements from this list. 
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
            size = 0;
        }
    }

    /**
     * Returns the element at the specified position in this list.
     */
    @Override
    public Object get(int index) {
        rangeCheck (index);
        return elements [index];
    }

    /**
     * Returns the index in this list of the first occurrence of the specified
     * element, or -1 if this list does not contain this element.
     */
    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Returns <tt>true</tt> if this list contains no elements.
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Removes the element at the specified position in this list.
     */
    @Override
    public Object remove(int index) {
        outOfBound(index);
        Object oldElement = elements[index];
        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     */
    @Override
    public Object set(int index, Object element) {
        rangeCheck (index);
        Object oldValue = elements [index];
        elements [index] = element;
        return oldValue;
    }

    /**
     * Returns the number of elements in this list.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * To confirm array capacity
     *
     * @param minCapacity
     */
    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }

    /**
     * To confirm the array capacity before adding a new element
     *
     * @param minCapacity
     */
    private void rangeCheckForAdd(int index) {
        if (index > size|| index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }
    
    
     private void rangeCheck(int index) {
        if (index > size - 1 || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * To see if the object was found
     *
     * @param index
     */
    private void outOfBound(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        }
    }

}
